import {RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardDetailComponent } from './dashboard-detail/dashboard-detail.component';
import { DashboardCreateComponent } from './dashboard-create/dashboard-create.component';
import { DashboardEditComponent } from './dashboard-edit/dashboard-edit.component';
const appRoutes: Routes = [
    {
      path: 'dashboard',
      component: DashboardComponent,
      data: { title: 'Emp List' }
    },
    {
      path: 'dashboard-details/:id',
      component: DashboardDetailComponent,
      data: { title: 'Emp Details' }
    },
    {
      path: 'dashboard-create',
      component: DashboardCreateComponent,
      data: { title: 'Create Emp' }
    },
    {
      path: 'dashboard-edit/:id',
      component: DashboardEditComponent,
      data: { title: 'Edit Emp' }
    },
    { path: '',
      redirectTo: '/dashboard',
      pathMatch: 'full'
    }
  ];
@NgModule({
    imports: [
      RouterModule.forRoot(appRoutes, {
        scrollPositionRestoration: 'enabled',
        anchorScrolling: 'enabled'
      })
    ],
    exports: [
      RouterModule
    ]
  })
    export class AppRouteModule {
    }
    export const routingComponents = [
        DashboardComponent,
        DashboardCreateComponent,
        DashboardDetailComponent,
        DashboardEditComponent
    ];