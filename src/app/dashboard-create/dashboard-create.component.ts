import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataserviceService } from '../dataservice.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
@Component({
  selector: 'app-dashboard-create',
  templateUrl: './dashboard-create.component.html',
  styleUrls: ['./dashboard-create.component.scss']
})
export class DashboardCreateComponent implements OnInit {
  EmpForm: FormGroup;
  id: String = '';
  Name: String = '';
  Desigination: String = '';
  Experience: String = '';
  Domain: String = '';
  DOJ: Date = new Date();
  matcher = new ErrorStateMatcher;
  constructor(private router: Router, private api: DataserviceService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.EmpForm = this.formBuilder.group({
      'Name' : ['', Validators.required],
      'Desigination' : ['', Validators.required],
      'Experience' : ['', Validators.required],
      'Domain' : ['', Validators.required],
      'DOJ' : [new Date(), Validators.required]
    });
  }
  onFormSubmit(form:NgForm) {
    this.api.postData(form)
      .subscribe(res => {
          let id = res['_id'];
          this.router.navigate(['/dashboard-details', id]);
        }, (err) => {
          console.log(err);
        });
  }

}
