import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataserviceService } from '../dataservice.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
@Component({
  selector: 'app-dashboard-edit',
  templateUrl: './dashboard-edit.component.html',
  styleUrls: ['./dashboard-edit.component.scss'],
  providers: [MatDatepickerModule]
})
export class DashboardEditComponent implements OnInit {

  constructor(private api: DataserviceService, private router: Router, private route: ActivatedRoute,
    private formBuilder: FormBuilder, private datepicker: MatDatepickerModule) {
    }
    id: String = '';
    EmpForm: FormGroup;
    Name: String = '';
    Desigination: String = '';
    Experience: String = '';
    Domain: String = '';
    DOJ: Date = new Date();
    matcher = new ErrorStateMatcher();

  ngOnInit() {
    this.getEmpList(this.route.snapshot.params['id']);
    this.EmpForm = this.formBuilder.group({
      'Name' : [null, Validators.required],
      'Desigination' : [null, Validators.required],
      'Experience' : [null, Validators.required],
      'Domain' : [null, Validators.required],
      'DOJ' : [null, Validators.required]
  });
}
getEmpList(id) {
  this.api.getData(id).subscribe(data => {
    this.id = data._id;
    this.EmpForm.setValue({
      Name: data.Name,
      Desigination: data.Desigination,
      Experience: data.Experience,
      Domain: data.Domain,
      DOJ: data.DOJ
    });
  });
}
onFormSubmit(form:NgForm) {
  this.api.updateData(this.id, form)
    .subscribe(res => {
        let id = res['_id'];
        this.router.navigate(['/dashboard-details', id]);
      }, (err) => {
        console.log(err);
      }
    );
}
EmpListDetails() {
  this.router.navigate(['/dashboard-details', this.id]);
}
}
