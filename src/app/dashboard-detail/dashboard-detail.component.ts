import { Component, OnInit } from '@angular/core';
import { DataserviceService } from '../dataservice.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-dashboard-detail',
  templateUrl: './dashboard-detail.component.html',
  styleUrls: ['./dashboard-detail.component.scss'],

})
export class DashboardDetailComponent implements OnInit {
  empData = {};
  constructor(private route: ActivatedRoute, private api: DataserviceService, private router: Router) { }

  getBookDetails(id) {
    this.api.getData(id)
      .subscribe(data => {
        console.log(data);
        this.empData = data;
      });
  }
  deleteEmpList(id) {
    this.api.deleteData(id)
      .subscribe(res => {
          this.router.navigate(['/dashboard']);
        }, (err) => {
          console.log(err);
        }
      );
  }
  ngOnInit() {
    this.getBookDetails(this.route.snapshot.params['id']);
  }

}
