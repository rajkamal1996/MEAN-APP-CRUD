import { Component, OnInit } from '@angular/core';
import { DataserviceService } from '../dataservice.service';
import { DataSource } from '@angular/cdk/collections';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [DataserviceService]
})
export class DashboardComponent implements OnInit {
EmpList : any;
displayedColumns = [ 'Name', 'Desigination', 'DOJ', 'Domain', 'Experience'];
dataSource : any = [];
  constructor(public dataservice : DataserviceService) { }

  ngOnInit() {
    this.dataservice.getallData()
      .subscribe(res => {
        console.log(res);
        this.EmpList = res;
        console.log("Data"+ res);       
        this.dataSource = this.EmpList;
      }, err => {
        console.log(err);
      });
  }



}
export class EmpDataSource extends DataSource<any> {
  constructor(private dataservice: DataserviceService) {
    super()
  }

  connect() {
    return this.dataservice.getallData();
  }

  disconnect() {

  }
}