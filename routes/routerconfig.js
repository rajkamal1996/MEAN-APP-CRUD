var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var datamodel = require('../models/datamodel.js');

/* GET ALL datamodelS */
router.get('/', function(req, res, next) {
  datamodel.find(function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

/* GET SINGLE datamodel BY ID */
router.get('/:id', function(req, res, next) {
  datamodel.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* SAVE datamodel */
router.post('/', function(req, res, next) {
  datamodel.create(req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* UPDATE datamodel */
router.put('/:id', function(req, res, next) {
  datamodel.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE datamodel */
router.delete('/:id', function(req, res, next) {
  datamodel.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;