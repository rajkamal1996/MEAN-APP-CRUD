var mongoose = require('mongoose');
var CrudSchema = new mongoose.Schema({
    Name: String,
    Desigination: String,
    DOJ: { type: Date, default: Date.now },
    Experience: String,
    Domain: String,
    updated_date: { type: Date, default: Date.now },
  });   

  module.exports = mongoose.model('datamodel', CrudSchema);